import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsFormPersona06Demo-styles.js';
import '@bbva-web-components/bbva-form-field/bbva-form-field';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements/BaseElement';
import  stylesButtons  from '@bbva-commons-web-components/cells-base-elements/ButtonsCss';
import '@bbva-web-components/bbva-form-textarea/bbva-form-textarea';
import '@bbva-commons-web-components/cells-combobox/cells-combobox';

/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-form-persona-06-demo></cells-form-persona-06-demo>
```

##styling-doc

@customElement cells-form-persona-06-demo
*/
export class CellsFormPersona06Demo extends BaseElement {
  static get is() {
    return 'cells-form-persona-06-demo';
  }

  // Declare properties
  static get properties() {
    return {
      docTypes: Array,
      persona: Object
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.docTypes = [];
    this.addListeners();
  }

  async addListeners() {
    await this.updateComplete;
    const inputs = this.elementsAll('bbva-form-field, bbva-form-textarea');
    console.log('inputs', inputs);
    inputs.forEach(input => {
      input.addEventListener('input',({target}) => this.buildPersona(target));
    });
  }

  buildPersona(input) {
    let tempPersona = { ...this.persona };
    tempPersona[input.name] = input.value;
    this.persona = { ...tempPersona };
}

  static get styles() {
    return [
      styles,
      stylesButtons,
      getComponentSharedStyles('cells-form-persona-06-demo-shared-styles')
    ];
  }

  registrar() {
    let tipoDocumento = this.element('cells-combobox').getSelected();
    this.persona['tipoDocumento'] = tipoDocumento.value;
    this.requestUpdate();
    console.log('PersonaForm->', this.persona);
    this.dispatch('on-register-person', this.persona);
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <bbva-form-field class="inputs-form" name="nombres" optional-label="" label="Nombres" ></bbva-form-field>
      <bbva-form-field class="inputs-form" name="apellidos" optional-label="" label="Apellidos" ></bbva-form-field>
      <bbva-form-field class="inputs-form" name="telefono" optional-label="" label="Telefono" ></bbva-form-field>
      <bbva-form-field class="inputs-form" name="email" optional-label="" label="Email" ></bbva-form-field>
      <cells-combobox
          class="inputs-form"
          name="tipoDocumento"
          label="Tipo de documento"
          .items="${this,this.docTypes}"
          ></cells-combobox>
      <bbva-form-field class="inputs-form" name="numeroDocumento" optional-label="" label="Número de documento" ></bbva-form-field>
      <bbva-form-textarea class="inputs-form" name="descripcion" label="Descripciòn"></bbva-form-textarea>
      <button @click="${this.registrar}" class="button button-blue lg full-width" >Registrar</button>

    `;
  }
}
